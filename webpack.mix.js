let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.setPublicPath('./src/Public');

mix.setResourceRoot('/jobs/');

mix.sass('src/Resources/Assets/sass/jobs-app.scss', 'css/jobs-app.css')
    .js('src/Resources/Assets/js/jobs-app.js', 'js/jobs-app.js')
    .version();


