# Laravel Job Monitor

> Monitoring stat board to check success of queued / sync jobs

- Statistics Board
- Auto recording of successful jobs

## Installation

- Install is easy as it uses Laravel auto discover

```shell
composer require elmhurstprojects/laravel-jobs
```

- Then publish the assets

```shell
php artisan vendor:publish --tag=jobs
```

- Then migrate the complete jobs table

```shell
php artisan migrate
```

## Usage

> Completed jobs are stored in the migrated table, using this package simply required looking at the monitoring views. 

- Visit url your-domain.com/jobs/overview