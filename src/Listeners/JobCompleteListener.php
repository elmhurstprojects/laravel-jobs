<?php

namespace ElmhurstProjects\Jobs\Listeners;

use ElmhurstProjects\Jobs\Models\JobCompleted;
use ElmhurstProjects\Jobs\Traits\GetVariablesTrait;

class JobCompleteListener
{
    use GetVariablesTrait;

    /**
     * Whenever a job is proccessed it is stored into the database
     * @param \Illuminate\Queue\Events\JobProcessed $event
     */
    public function handle(\Illuminate\Queue\Events\JobProcessed $event)
    {
        $commandName = $event->job->resolveName();

        try {
            $variables = serialize($this->setVariablesFromCompletedJobPayload($event->job->payload()));
        } catch (\Exception $e) {
            $variables = $e->getMessage();
        }

        try {
            JobCompleted::create([
                'connection' => $event->job->getConnectionName(),
                'queue' => $event->job->getQueue(),
                'payload' => serialize($event->job->payload()),
                'job' => substr($commandName, strrpos($commandName, "\\") + 1),
                'variables' => $variables
            ]);
        } catch (\Exception $e) {
            \Log::error($e->getMessage() . ' >>> ' . serialize($event));
        }

    }

    /**
     * Extract the variables from the completed job
     * @param array $payload
     * @return array|null
     */
    protected function setVariablesFromCompletedJobPayload(array $payload): array
    {
        try {
            $payload = unserialize($payload['data']['command']);

            $variables = get_object_vars($payload);

            return $this->unsetGenericVariables($variables);
        } catch (\Exception $e) {
            return ['Variables Not Known' => $e->getMessage()];
        }
    }
}