<?php namespace ElmhurstProjects\Jobs\Providers;

use Illuminate\Support\ServiceProvider;

class JobsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (!$this->app->routesAreCached()) {
            require_once __DIR__ . '/../Http/Routes/routes.php';
        }

        $this->loadViewsFrom(__DIR__.'/../Resources/Views/', 'jobs');

        if ($this->app->runningInConsole()) {
            $this->commands([

            ]);
        }

        $this->publishes([
            __DIR__.'/../Public' => public_path('jobs'),
        ], 'jobs');

        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(JobEventsServiceProvider::class);
    }
}
