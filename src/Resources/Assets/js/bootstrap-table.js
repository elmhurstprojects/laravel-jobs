window.ajaxOptions = {headers: {"X-CSRF-TOKEN": CSRF.get_token()}};

require('jquery');
require('bootstrap-table');
require('tableexport.jquery.plugin');
require('./csrf');
require('../../../../node_modules/bootstrap-table/dist/extensions/export/bootstrap-table-export');
require('../../../../node_modules/bootstrap-table/dist/extensions/filter-control/bootstrap-table-filter-control');

$.extend($.fn.bootstrapTable.defaults, {
    pagination: true,
    sortable: true,
    search: true,
    showColumns: true,
    showExport: true,
    exportTypes: ['csv'],
    pageSize:100,
    pageList:'[50, 100, 250, 500, All]',
    icons: {
        export: 'icon-download',
        columns: 'icon-equalizer',
        clear: 'icon-bin'
    },
    ajaxOptions: ajaxOptions,
    reorderableColumns: true,
    classes: "table table-bordered table-striped table-sm table-dark"
});




