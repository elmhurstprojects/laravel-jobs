$(document).ready(function(){
    $('body').on('click', '.js-dispatch-now', function (e) {
        e.preventDefault();

        let data = {
            _token: CSRF.get_token(),
            job_id: $(this).attr('data-job_id')
        }

        let self = $(this);

        $.post('/jobs/dispatch-now', data, function (response) {
            console.log(response);

            console.log(self.attr('data-job_id'));

            self.html('Dispatched').removeClass('js-dispatch-now').attr('disable');

            $('#drilldown-jobs-outstanding').bootstrapTable('remove', {field: 'id', values: [self.attr('data-job_id')]});
        });
    })

    $('body').on('click', '.js-delete', function (e) {
        e.preventDefault();

        let data = {
            _token: CSRF.get_token(),
            job_id: $(this).attr('data-job_id')
        }

        let self = $(this);

        $.post('/jobs/delete', data, function (response) {
            console.log(response);

            self.html('Deleted').removeClass('js-delete').attr('disable');

            $('#drilldown-jobs-outstanding').bootstrapTable('remove', {field: 'id', values: [self.attr('data-job_id')]});
        });
    })
})