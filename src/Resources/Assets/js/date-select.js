require('../../../../node_modules/jquery-ui-dist/jquery-ui.js');

$(document).ready(function(){
    $('.js-datepicker').datepicker({ dateFormat: 'yy-mm-dd' });

    $('.js-prefix-date').change(function(){
        var start_date = $(this).find(':selected').data('start');
        var end_date = $(this).find(':selected').data('end');
        $('#start_date').val(start_date);
        $('#end_date').val(end_date);
    });
});