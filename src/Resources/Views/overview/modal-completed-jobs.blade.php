<div id="completedJobsModal" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 1500px !important;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Completed Jobs Breakdown</h4>
            </div>
            <div class="modal-body" id="completedJobsModalContent">
                <table id="drilldown-jobs-completed" class="table" data-toggle="table" data-filter-control="true">
                    <thead>
                    <tr>
                        <th data-field="id" data-sortable="true" data-filter-control="input">ID</th>
                        <th data-field="completed_at" data-sortable="true" data-filter-control="input">Completed At</th>
                        <th data-field="queue" data-sortable="true" data-filter-control="select">Queue</th>
                        <th data-field="object" data-sortable="true" data-filter-control="select">Object</th>
                        <th data-field="variables" data-sortable="true" data-filter-control="select">Variables</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>