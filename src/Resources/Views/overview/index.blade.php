@extends('jobs::layouts.noSidebar')

@section('subheading')
    Overview
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            @include('jobs::overview.modal-pending-jobs')
            @include('jobs::overview.modal-stalled-jobs')
            @include('jobs::overview.modal-failed-jobs')
            @include('jobs::overview.modal-completed-jobs')
        </div>
    </div>

    <div class="row">
        <div class="col-xs-3">
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Pending Jobs
                        </div>
                        <div class="panel-body">
                            <h1 class="text-center" style="font-size: 10rem">{!! $pending_job_total !!}</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <table class="table table-bordered table-hover table-stripped">
                        <thead>
                        <tr>
                            <th>Queue</th>
                            <th>Pending Total</th>
                            <th>Options</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($pending_job_queue_breakdown as $breakdown)
                            <tr>
                                <td>{!! $breakdown->queue !!}</td>
                                <td>{!! $breakdown->total_jobs !!}</td>
                                <td>
                                    <a href="#" class="btn btn-xs btn-primary js-view-pending-breakdown" data-queue="{!! $breakdown->queue !!}">
                                        View Breakdown
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-xs-3">
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            Stalled Jobs
                        </div>
                        <div class="panel-body">
                            <h1 class="text-center" style="font-size: 10rem">{!! $stalled_job_total !!}</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <table class="table table-bordered table-hover table-stripped">
                        <thead>
                        <tr>
                            <th>Queue</th>
                            <th>Stalled Total</th>
                            <th>Options</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($stalled_job_queue_breakdown as $breakdown)
                            <tr>
                                <td>{!! $breakdown->queue !!}</td>
                                <td>{!! $breakdown->total_jobs !!}</td>
                                <td>
                                    <a href="#" class="btn btn-xs btn-primary js-view-stalled-breakdown" data-queue="{!! $breakdown->queue !!}">
                                        View Breakdown
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-xs-3">
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            Failed Jobs
                        </div>
                        <div class="panel-body">
                            <h1 class="text-center" style="font-size: 10rem">{!! $failed_job_total !!}</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <input type="hidden" id="start_date" value="{!! \Carbon\Carbon::now()->startOfDay()->format('Y-m-d H:s') !!}">
                    <input type="hidden" id="end_date" value="{!! \Carbon\Carbon::now()->endOfDay()->format('Y-m-d H:s') !!}">
                    <table class="table table-bordered table-hover table-stripped">
                        <thead>
                        <tr>
                            <th>Queue</th>
                            <th>Failed Total</th>
                            <th>Options</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($failed_job_queue_breakdown as $breakdown)
                            <tr>
                                <td>{!! $breakdown->queue !!}</td>
                                <td>{!! $breakdown->total_jobs !!}</td>
                                <td>
                                    <a href="#" class="btn btn-xs btn-primary js-view-failed-breakdown" data-queue="{!! $breakdown->queue !!}">
                                        View Breakdown
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>




        <div class="col-xs-3">
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            Completed Jobs
                        </div>
                        <div class="panel-body">
                            <h1 class="text-center" style="font-size: 10rem">{!! $completed_job_total !!}</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <input type="hidden" id="start_date" value="{!! \Carbon\Carbon::now()->startOfDay()->format('Y-m-d H:s') !!}">
                    <input type="hidden" id="end_date" value="{!! \Carbon\Carbon::now()->endOfDay()->format('Y-m-d H:s') !!}">
                    <table class="table table-bordered table-hover table-stripped">
                        <thead>
                        <tr>
                            <th>Queue</th>
                            <th>Completed Total</th>
                            <th>Options</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($completed_job_queue_breakdown as $breakdown)
                            <tr>
                                <td>{!! $breakdown->queue !!}</td>
                                <td>{!! $breakdown->total_jobs !!}</td>
                                <td>
                                    <a href="#" class="btn btn-xs btn-primary js-view-completed-breakdown" data-queue="{!! $breakdown->queue !!}">
                                        View Breakdown
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
@stop


@section('page-scripts')
    <script>
        $(document).ready(function(){
            $('body').on('click', '.js-view-pending-breakdown', function(e){
                e.preventDefault();
                $('#pendingJobsModal').modal('show');

                $('#drilldown-jobs-pending').bootstrapTable('refresh', {
                    url: '/drilldowns/jobs-pending',
                    query: {
                        start_date: $('#start_date').val(),
                        end_date: $('#end_date').val(),
                        queue: $(this).attr('data-queue')
                    }
                });
            });

            $('body').on('click', '.js-view-stalled-breakdown', function(e){
                e.preventDefault();
                $('#stalledJobsModal').modal('show');

                $('#drilldown-jobs-stalled').bootstrapTable('refresh', {
                    url: '/drilldowns/jobs-stalled',
                    query: {
                        start_date: $('#start_date').val(),
                        end_date: $('#end_date').val(),
                        queue: $(this).attr('data-queue')
                    }
                });
            });

            $('body').on('click', '.js-view-failed-breakdown', function(e){
                e.preventDefault();
                $('#failedJobsModal').modal('show');

                $('#drilldown-jobs-failed').bootstrapTable('refresh', {
                    url: '/drilldowns/jobs-failed',
                    query: {
                        start_date: $('#start_date').val(),
                        end_date: $('#end_date').val(),
                        queue: $(this).attr('data-queue')
                    }
                });
            });

            $('body').on('click', '.js-view-completed-breakdown', function(e){
                e.preventDefault();
                $('#completedJobsModal').modal('show');

                $('#drilldown-jobs-completed').bootstrapTable('refresh', {
                    url: '/drilldowns/jobs-completed',
                    query: {
                        start_date: $('#start_date').val(),
                        end_date: $('#end_date').val(),
                        queue: $(this).attr('data-queue')
                    }
                });
            });
        })

        setTimeout(function(){
            window.location.reload();
        }, 180000);
    </script>

@stop