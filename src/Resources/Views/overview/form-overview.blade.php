<div class="panel panel-default">
    <div class="panel-body">
        <form action="{!! route('jobs.overview.post') !!}" method="post" class="form-inline" autocomplete="off">
            {!! csrf_field() !!}
            <div class="form-group">
                <select name="prefix_date" id="prefix_date" class="form-control js-prefix-date">
                    <option>Select Prefix Date</option>
                    <option data-start="{!! \Carbon\Carbon::now()->startOfDay()->format('Y-m-d H:i') !!}"
                            data-end="{!! \Carbon\Carbon::now()->endOfDay()->format('Y-m-d H:i') !!}">Today
                    </option>
                    <option data-start="{!! \Carbon\Carbon::now()->subDay()->startOfDay()->format('Y-m-d H:i') !!}"
                            data-end="{!! \Carbon\Carbon::now()->subDay()->endOfDay()->format('Y-m-d H:i') !!}">
                        Yesterday
                    </option>
                    <option data-start="{!! \Carbon\Carbon::now()->subDays(7)->startOfDay()->format('Y-m-d H:i') !!}"
                            data-end="{!! \Carbon\Carbon::now()->endOfDay()->format('Y-m-d H:i') !!}">Last 7 Days
                    </option>
                    <option data-start="{!! \Carbon\Carbon::now()->startOfWeek()->format('Y-m-d H:i') !!}"
                            data-end="{!! \Carbon\Carbon::now()->endOfDay()->format('Y-m-d H:i') !!}">This week
                    </option>
                    <option data-start="{!! \Carbon\Carbon::now()->subWeek()->startOfWeek()->format('Y-m-d H:i') !!}"
                            data-end="{!! \Carbon\Carbon::now()->subWeek()->endOfWeek()->format('Y-m-d H:i') !!}">
                        Last week
                    </option>
                    <option data-start="{!! \Carbon\Carbon::now()->previous(\Carbon\Carbon::SUNDAY)->subDays(1)->startOfDay()->format('Y-m-d H:i') !!}"
                            data-end="{!! \Carbon\Carbon::now()->previous(\Carbon\Carbon::SUNDAY)->endOfDay()->format('Y-m-d H:i') !!}">
                        Last Weekend
                    </option>
                    <option data-start="{!! \Carbon\Carbon::now()->subDays(30)->startOfDay()->format('Y-m-d H:i') !!}"
                            data-end="{!! \Carbon\Carbon::now()->endOfDay()->format('Y-m-d H:i') !!}">Last 30 Days
                    </option>

                    <option data-start="{!! \Carbon\Carbon::now()->startOfMonth()->format('Y-m-d H:i') !!}"
                            data-end="{!! \Carbon\Carbon::now()->endOfDay()->format('Y-m-d H:i') !!}">This Month
                    </option>
                    <option data-start="{!! \Carbon\Carbon::now()->subMonth()->startOfMonth()->format('Y-m-d H:i') !!}"
                            data-end="{!! \Carbon\Carbon::now()->subMonth()->endOfMonth()->format('Y-m-d H:i') !!}">
                        Last Month
                    </option>
                </select>
            </div>

            <div class="form-group">
                <input type="text" id="start_date" name="start_date" class="form-control js-datepicker"
                       placeholder="Start Date"
                       value="@if(isset($request) && $request->start_date != '') {!!$request->start_date !!} @endif">
            </div>
            <div class="form-group">
                <input type="text" id="end_date" name="end_date" class="form-control js-datepicker"
                       placeholder="End Date"
                       value="@if(isset($request) && $request->end_date != '') {!!$request->end_date !!} @endif">
            </div>
            <div class="form-group pull-right">
                <input type="submit" value="Search Jobs" class="btn btn-primary">
            </div>
        </form>
    </div>
</div>
