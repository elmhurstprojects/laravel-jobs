@extends('jobs::layouts.noSidebar')

@section('subheading')
    Outstanding Jobs Index
@stop

@section('content')
    <div class="row">
        <div class="col-xs-7 col-xs-offset-5">
            @include('jobs::partials.form-future', ['url' => 'jobs.outstanding.post', 'request' => $request, 'queues' => $queues])
        </div>
    </div>

    <div class="row space-1">
        <div class="col-xs-12">
                <table id="drilldown-jobs-outstanding" class="table" data-toggle="table" data-filter-control="true">
                    <thead>
                    <tr>
                        <th data-field="id" data-sortable="true" data-filter-control="input">ID</th>
                        <th data-field="created_at" data-sortable="true" data-filter-control="input">Created</th>
                        <th data-field="available_at" data-sortable="true" data-filter-control="input">Available</th>
                        <th data-field="queue" data-sortable="true" data-filter-control="select">Queue</th>
                        <th data-field="attempts" data-sortable="true" data-filter-control="select">Attempts</th>
                        <th data-field="object" data-sortable="true" data-filter-control="select">Object</th>
                        <th data-field="variables" data-sortable="true" data-filter-control="input">Variables</th>
                        <th data-field="options">Options</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
        </div>
    </div>
@stop


@section('page-scripts')

    <script>
        $(document).ready(function () {
            $('#drilldown-jobs-outstanding').bootstrapTable('refresh', {
                url: '/drilldowns/jobs-outstanding',
                query: {
                    start_date: $('#start_date').val(),
                    end_date: $('#end_date').val(),
                    queue: $('#queue').val()
                }
            });

            $('body').on('click', '.js-view-outstanding-breakdown', function(e){
                e.preventDefault();
                $('#outstandingJobsModal').modal('show');

                $('#drilldown-jobs-outstanding').bootstrapTable('refresh', {
                    url: '/drilldowns/jobs-outstanding',
                    query: {
                        start_date: $('#start_date').val(),
                        end_date: $('#end_date').val(),
                        queue: $(this).attr('data-queue')
                    }
                });
            });

            $('body').on('click', '.js-view-failed-breakdown', function(e){
                e.preventDefault();
                $('#failedJobsModal').modal('show');

                $('#drilldown-jobs-failed').bootstrapTable('refresh', {
                    url: '/drilldowns/jobs-failed',
                    query: {
                        start_date: $('#start_date').val(),
                        end_date: $('#end_date').val(),
                        queue: $(this).attr('data-queue')
                    }
                });
            });
        });
    </script>
@stop