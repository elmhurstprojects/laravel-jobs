@extends('jobs::layouts.noSidebar')

@section('subheading')
    Completed Jobs Index
@stop

@section('content')
    <div class="row">
        <div class="col-xs-7 col-xs-offset-5">
            @include('jobs::partials.form-past', ['url' => 'jobs.completed.post'])
        </div>
    </div>

    <div class="row space-1">
        <div class="col-xs-12">
            <table class="table" data-toggle="table">
                <thead>
                <tr>
                    <th data-field="id" data-sortable="true" data-filter-control="input">ID</th>
                    <th data-field="completed_at" data-sortable="true" data-filter-control="input">Completed At</th>
                    <th data-field="queue" data-sortable="true" data-filter-control="select">Queue</th>
                    <th data-field="object" data-sortable="true" data-filter-control="select">Object</th>
                    <th data-field="variables" data-sortable="true" data-filter-control="select">Variables</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>

        </div>
    </div>
@stop


@section('page-scripts')
    <script>
        $(document).ready(function () {
            $('#drilldown-jobs-completed').bootstrapTable('refresh', {
                url: '/drilldowns/jobs-completed',
                query: {
                    start_date: $('#start_date').val(),
                    end_date: $('#end_date').val(),
                    queue: $('#queue').val(),
                }
            });
        });
    </script>
@stop