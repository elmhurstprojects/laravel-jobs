@extends('jobs::layouts.noSidebar')

@section('content')
    <div class="row">
        <div class="col-xs-7 col-xs-offset-5">
            @include('jobs::partials.form-past', ['url' => 'jobs.failed.post', 'request' => $request, 'queues' => $queues])
        </div>
    </div>

    <div class="row space-1">
        <div class="col-xs-12">
            <table id="drilldown-jobs-failed" class="table" data-toggle="table" data-filter-control="true">
                <thead>
                <tr>
                    <th data-field="id" data-sortable="true" data-filter-control="input">ID</th>
                    <th data-field="failed_at" data-sortable="true" data-filter-control="input">Failed At</th>
                    <th data-field="queue" data-sortable="true" data-filter-control="select">Queue</th>
                    <th data-field="object" data-sortable="true" data-filter-control="select">Object</th>
                    <th data-field="exception" data-sortable="true" data-filter-control="select">Exception</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
@stop


@section('page-scripts')

    <script>
        $(document).ready(function () {
            $('#drilldown-jobs-failed').bootstrapTable('refresh', {
                url: '/drilldowns/jobs-failed',
                query: {
                    start_date: $('#start_date').val(),
                    end_date: $('#end_date').val(),
                    queue: $('#queue').val(),
                }
            });
        });
    </script>
@stop