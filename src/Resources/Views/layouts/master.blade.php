<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>

    <meta name="csrf_token" content="{!! csrf_token() !!}"/>
    <meta name="csrf-token" content="{!! csrf_token() !!}"/>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <link rel="stylesheet" href="{!! asset(mix('css/jobs-app.css', 'jobs')) !!}">
    @stack('headcss')

    @yield('meta')

</head>
<body>

@stack('modals')

@if (isset($errors) && $errors->any())
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endif

<div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <section class="alerts">
                    <div class="alert alert-success" id="js-alert-success" role="alert" style="display:none">
                        {{-- Container for js text --}}
                    </div>

                    @if(session()->has('success'))
                        <div class="alert alert-success" role="alert">
                            {!! session()->get('success')!!}
                        </div>
                    @endif

                    @if(session()->has('error'))
                        <div class="alert alert-danger" role="alert">
                            {!! session()->get('error') !!}
                        </div>
                    @endif

                    @if(session()->has('warn'))
                        <div class="alert alert-warn" role="alert">
                            {!! session()->get('warn') !!}
                        </div>
                    @endif
                </section>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        @yield('main')
    </div>
</div>

<script src="{!! mix('js/jobs-app.js', 'jobs') !!}"></script>

@yield('page-scripts')

</body>
</html>
