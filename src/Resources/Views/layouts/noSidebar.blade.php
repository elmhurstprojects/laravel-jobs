@extends('jobs::layouts.master')

@section('main')
    @include('jobs::partials.menu')

    <div class="row">
        <div class="col-xs-12">
            @yield('content')
        </div>
    </div>
@stop