<div class="panel panel-default">
    <div class="panel-body">
        <form action="{!! route($url) !!}" method="post" class="form-inline" autocomplete="off">
            {!! csrf_field() !!}
            <div class="form-group">
                <select name="prefix_date" id="prefix_date" class="form-control js-prefix-date">
                    <option>Select Prefix Date</option>
                    <option data-start="{!! \Carbon\Carbon::now()->subYears(10)->format('Y-m-d H:i') !!}"
                            data-end="{!! \Carbon\Carbon::now()->addMinutes(5)->format('Y-m-d H:i') !!}">
                        Next 5 Minutes
                    </option>

                    <option data-start="{!! \Carbon\Carbon::now()->subYears(10)->format('Y-m-d H:i') !!}"
                            data-end="{!! \Carbon\Carbon::now()->addMinutes(10)->format('Y-m-d H:i') !!}">
                        Next 10 Minutes
                    </option>

                    <option data-start="{!! \Carbon\Carbon::now()->subYears(10)->format('Y-m-d H:i') !!}"
                            data-end="{!! \Carbon\Carbon::now()->addMinutes(30)->format('Y-m-d H:i') !!}">
                        Next Half Hour
                    </option>

                    <option data-start="{!! \Carbon\Carbon::now()->subYears(10)->format('Y-m-d H:i') !!}"
                            data-end="{!! \Carbon\Carbon::now()->addMinutes(60)->format('Y-m-d H:i') !!}">
                        Next Hour
                    </option>

                    <option data-start="{!! \Carbon\Carbon::now()->startOfDay()->format('Y-m-d H:i') !!}"
                            data-end="{!! \Carbon\Carbon::now()->endOfDay()->format('Y-m-d H:i') !!}">
                        Today
                    </option>

                    <option data-start="{!! \Carbon\Carbon::now()->startOfWeek()->format('Y-m-d H:i') !!}"
                            data-end="{!! \Carbon\Carbon::now()->endOfDay()->format('Y-m-d H:i') !!}">This week
                    </option>

                    <option data-start="{!! \Carbon\Carbon::now()->startOfMonth()->format('Y-m-d H:i') !!}"
                            data-end="{!! \Carbon\Carbon::now()->endOfDay()->format('Y-m-d H:i') !!}">This Month
                    </option>
                </select>
            </div>

            <div class="form-group">
                <select name="queue" id="queue" class="form-control">
                    <option value="all">Select Queue</option>
                    @foreach($queues as $queue)
                        <option value="{!! $queue !!}" @if(isset($request->queue) && $request->queue == $queue) selected="selected" @endif>
                            {!! $queue !!}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <input type="text" id="start_date" name="start_date" class="form-control js-datepicker" placeholder="Start Date" value="@if(isset($request) && $request->start_date != '') {!!$request->start_date !!} @endif">
            </div>
            <div class="form-group">
                <input type="text" id="end_date" name="end_date" class="form-control js-datepicker" placeholder="End Date"  value="@if(isset($request) && $request->end_date != '') {!!$request->end_date !!} @endif">
            </div>
            <div class="form-group pull-right">
                <input type="submit" value="Search Jobs" class="btn btn-primary">
            </div>
        </form>
    </div>
</div>
