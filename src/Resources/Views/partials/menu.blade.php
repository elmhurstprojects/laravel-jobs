<div class="row">
    <div class="col-xs-12">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Laravel Job Monitor</a>
                </div>
                <ul class="nav navbar-nav">
                    <li><a href="{!! route('jobs.overview') !!}">Overview</a></li>
                    <li><a href="{!! route('jobs.outstanding') !!}">Outstanding</a></li>
                    <li><a href="{!! route('jobs.failed') !!}">Failed</a></li>
                    <li><a href="{!! route('jobs.completed') !!}">Completed</a></li>
                </ul>
            </div>
        </nav>
    </div>
</div>