<?php

namespace ElmhurstProjects\Jobs\Managers;

use ElmhurstProjects\Jobs\Models\Job;

class OutstandingJobManager
{
    /**
     * Gets array of all queues in the job table
     * @return array
     */
    public function getQueues():array
    {
        return Job::where('id', '!=', null)->groupby('queue')->pluck('queue')->toArray();
    }
}
