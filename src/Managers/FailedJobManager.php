<?php

namespace ElmhurstProjects\Jobs\Managers;

use ElmhurstProjects\Jobs\Models\JobFailed;

class FailedJobManager
{
    /**
     * Gets array of all queues in the failed table
     * @return array
     */
    public function getFailedQueues():array
    {
        return JobFailed::where('id', '!=', null)->groupby('queue')->pluck('queue')->toArray();
    }
}
