<?php

namespace ElmhurstProjects\Jobs\Managers;

use ElmhurstProjects\Jobs\Models\Job;
use ElmhurstProjects\Jobs\Models\JobCompleted;
use ElmhurstProjects\Jobs\Models\JobFailed;
use ElmhurstProjects\Jobs\Traits\GetVariablesTrait;

class DisplayManager
{
    use GetVariablesTrait;

    /**
     * Returns the object name
     * @param JobFailed $jobFailed
     * @return string
     */
    public function getFailedJobObject(JobFailed $jobFailed): string
    {
        $temp = json_decode($jobFailed->payload);

        $commandName = $temp->data->commandName;

        return substr($commandName, strrpos($commandName, "\\") + 1);
    }

    /**
     * Get the exception based failed job
     * @param JobFailed $jobFailed
     * @return string
     */
    public function getFailedException(JobFailed $jobFailed): string
    {
        $arr = explode("Stack trace:", $jobFailed->exception, 2);

        return $arr[0];
    }

    /**
     * Returns outstanding job object name
     * @param Job $job
     * @return bool|string
     */
    public function getOutstandingJobObject(Job $job)
    {
        $temp = json_decode($job->payload);

        return (isset($temp->displayName))
            ? substr($temp->displayName, strrpos($temp->displayName, "\\") + 1)
            : 'Could not get job name';
    }

    /**
     * Returns outstanding job variables
     * @param Job $job
     * @return bool|string
     */
    public function getOutstandingJobVariables(Job $job):string
    {
        $string = '';

        foreach ($this->getVariablesFromOutstandingJobPayload($job->payload) as $key => $item) {
            $string .= $key . ' >>> ' . $item . '<br>';
        }

        return $string;
    }

    /**
     * Converts array to string to get variables for JobCompleted
     * @param JobCompleted $jobCompleted
     * @return string
     */
    public function getCompletedJobVariables(JobCompleted $jobCompleted): string
    {
        $string = '';

        if($jobCompleted->variables){
            foreach (unserialize($jobCompleted->variables) as $key => $item) {
                $string .= $key . ' >>> ' . $item . '<br>';
            }
        }else{
            try {
                $temp = json_decode($jobCompleted->payload);

                $command = unserialize($temp->data->command);

                $command_json = json_decode(json_encode($command));

                if (isset($command_json->event)) {
                    foreach (get_object_vars($command->event) as $key => $item) {
                        $string .= $key . ' >>> ' . $item . '<br>';
                    }
                } elseif (isset($command_json->tries)) {
                    $string = 'Tries: ' . $command_json->tries;
                }
            } catch (\Exception $e) {
                $string = 'Error getting variables: ' . $e->getMessage();
            }
        }

        return $string;
    }
}
