<?php

namespace ElmhurstProjects\Jobs\Managers;

use Carbon\Carbon;
use ElmhurstProjects\Jobs\Models\Job;
use ElmhurstProjects\Jobs\Models\JobCompleted;
use ElmhurstProjects\Jobs\Models\JobFailed;

class OverviewManager
{
    public function getOverview(Carbon $start_date, Carbon $end_date)
    {
        return (object)[
            'pending_job_queue_breakdown' => $this->getPendingJobsQueueBreakdown(),
            'pending_job_total' => $this->getPendingJobsTotal(),
            'stalled_job_queue_breakdown' => $this->getStalledJobsQueueBreakdown(),
            'stalled_job_total' => $this->getStalledJobsTotal(),
            'failed_job_queue_breakdown' => $this->getFailedJobsQueueBreakdown($start_date, $end_date),
            'failed_job_total' => $this->getFailedJobsTotal($start_date, $end_date),
            'completed_job_queue_breakdown' => $this->getCompletedJobsQueueBreakdown($start_date, $end_date),
            'completed_job_total' => $this->getCompletedJobsTotal($start_date, $end_date),
        ];
    }

    /**
     * Get pending job total
     * @return int
     */
    public function getPendingJobsTotal(): int
    {
        return Job::where('available_at', '>=', Carbon::now()->timestamp)->count();
    }

    /**
     * Gets the pending job breakdown
     * @return mixed
     */
    public function getPendingJobsQueueBreakdown()
    {
        return Job::select([
            \DB::raw('queue'),
            \DB::raw('COUNT(id) AS total_jobs')
        ])
            ->where('available_at', '>=', Carbon::now()->timestamp)
            ->groupby('queue')
            ->orderby('total_jobs', 'desc')
            ->get();
    }

    /**
     * Get stalled job total
     * @return int
     */
    public function getStalledJobsTotal(): int
    {
        return Job::where('available_at', '<', Carbon::now()->timestamp)->count();
    }

    /**
     * Gets the stalled job breakdown
     * @return mixed
     */
    public function getStalledJobsQueueBreakdown()
    {
        return Job::select([
            \DB::raw('queue'),
            \DB::raw('COUNT(id) AS total_jobs')
        ])
            ->where('available_at', '<', Carbon::now()->timestamp)
            ->groupby('queue')
            ->orderby('total_jobs', 'desc')
            ->get();
    }


    /**
     * Get failed job total
     * @param Carbon $start_date
     * @param Carbon $end_date
     * @return int
     */
    public function getFailedJobsTotal(Carbon $start_date, Carbon $end_date): int
    {
        return JobFailed::whereBetween('failed_at', [$start_date->toDateTimeString(), $end_date->toDateTimeString()])->count();
    }

    /**
     * Gets the failed job breakdown
     * @param Carbon $start_date
     * @param Carbon $end_date
     * @return mixed
     */
    public function getFailedJobsQueueBreakdown(Carbon $start_date, Carbon $end_date)
    {
        return JobFailed::select([
            \DB::raw('queue'),
            \DB::raw('COUNT(id) AS total_jobs')
        ])
            ->whereBetween('failed_at', [$start_date->toDateTimeString(), $end_date->toDateTimeString()])
            ->groupby('queue')
            ->orderby('total_jobs', 'desc')
            ->get();
    }

    /**
     * Get completed job total
     * @param Carbon $start_date
     * @param Carbon $end_date
     * @return int
     */
    public function getCompletedJobsTotal(Carbon $start_date, Carbon $end_date): int
    {
        return JobCompleted::whereBetween('created_at', [$start_date->toDateTimeString(), $end_date->toDateTimeString()])->count();
    }

    /**
     * Gets the completed job breakdown
     * @param Carbon $start_date
     * @param Carbon $end_date
     * @return mixed
     */
    public function getCompletedJobsQueueBreakdown(Carbon $start_date, Carbon $end_date)
    {
        return JobCompleted::select([
            \DB::raw('queue'),
            \DB::raw('COUNT(id) AS total_jobs')
        ])
            ->whereBetween('created_at', [$start_date->toDateTimeString(), $end_date->toDateTimeString()])
            ->groupby('queue')
            ->orderby('total_jobs', 'desc')
            ->get();
    }
}
