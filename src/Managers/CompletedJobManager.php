<?php

namespace ElmhurstProjects\Jobs\Managers;

use ElmhurstProjects\Jobs\Models\JobCompleted;

class CompletedJobManager
{
    /**
     * Gets array of all queues in the failed table
     * @return array
     */
    public function getCompletedQueues():array
    {
        return JobCompleted::where('id', '!=', null)->groupby('queue')->pluck('queue')->toArray();
    }
}
