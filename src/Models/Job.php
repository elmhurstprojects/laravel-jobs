<?php

namespace ElmhurstProjects\Jobs\Models;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $table = 'jobs';

    protected $guarded = ['id'];

    public $timestamps = false;
}
