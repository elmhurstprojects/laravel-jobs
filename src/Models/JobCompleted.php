<?php

namespace ElmhurstProjects\Jobs\Models;

use Illuminate\Database\Eloquent\Model;

class JobCompleted extends Model
{
    protected $table = 'jobs_completed';

    protected $guarded = ['id'];
}
