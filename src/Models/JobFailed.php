<?php

namespace ElmhurstProjects\Jobs\Models;

use Illuminate\Database\Eloquent\Model;

class JobFailed extends Model
{
    protected $table = 'failed_jobs';

    protected $guarded = ['id'];
}
