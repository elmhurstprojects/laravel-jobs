<?php

namespace ElmhurstProjects\Jobs\Http\Controllers\Drilldowns;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use ElmhurstProjects\Jobs\Managers\DisplayManager;
use ElmhurstProjects\Jobs\Models\Job;
use ElmhurstProjects\Jobs\Models\JobFailed;
use Global4Communications\Payment\Models\PaymentTransaction;
use Illuminate\Http\Request;

class JobsFailedController extends Controller
{
    protected $display_manager;

    public function __construct()
    {
        $this->display_manager = new DisplayManager();
    }

    public function json(Request $request)
    {
        $start_date = Carbon::createFromFormat('Y-m-d H:i', $request->get('start_date'))->startOfDay();

        $end_date = Carbon::createFromFormat('Y-m-d H:i', $request->get('end_date'))->endOfDay();

        $jobs = JobFailed::whereBetween('failed_at', [$start_date->format('Y-m-d H:i:s'), $end_date->format('Y-m-d H:i:s')])
            ->when($request->get('queue') != 'all', function ($query) use ($request) {
                return $query->where('queue', $request->get('queue'));
            })
            ->get();

        return response()->json(['request' => $request->all(), 'rows' => $this->rows($jobs)]);
    }


    protected function rows($jobs)
    {
        $rows = [];

        foreach ($jobs as $job) {
            $rows[] = [
                'id' => $job->id,
                'failed_at' => $job->failed_at,
                'queue' => $job->queue,
                'object' => $this->display_manager->getFailedJobObject($job),
                'exception' => $this->display_manager->getFailedException($job),
            ];
        }

        return $rows;
    }

    /**
     * Converts array to string
     * @param array $array
     * @return string
     */
    protected function arrayToString(array $array): string
    {
        $string = '';

        foreach ($array as $key => $item) {
            $string .= $key . ' >>> ' . $item . '<br>';
        }

        return $string;
    }

    /**
     * Get the option buttons
     * @param int $job_id
     * @return string
     */
    protected function optionButtons(int $job_id): string
    {
        return '<div class="btn-toolbar">
                                     <a href="#" class="btn btn-xs btn-primary js-dispatch-now" data-job_id="' . $job_id . '">
        Dispatch Now
    </a>
                                     <a href="#" class="btn btn-xs btn-danger js-delete" data-job_id="' . $job_id . '">
        Delete
                                     </a>
                                 </div>';
    }
}
