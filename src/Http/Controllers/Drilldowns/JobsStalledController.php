<?php

namespace ElmhurstProjects\Jobs\Http\Controllers\Drilldowns;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use ElmhurstProjects\Jobs\Managers\DisplayManager;
use ElmhurstProjects\Jobs\Models\Job;
use Global4Communications\Payment\Models\PaymentTransaction;
use Illuminate\Http\Request;

class JobsStalledController extends Controller
{
    protected $display_manager;

    public function __construct()
    {
        $this->display_manager = new DisplayManager();
    }

    public function json(Request $request)
    {
        $jobs = Job::where('available_at', '<', Carbon::now()->timestamp)
            ->when($request->get('queue') != 'all', function($query) use ($request){
                return $query->where('queue', $request->get('queue'));
            })
            ->get();

        return response()->json(['request' => $request->all(), 'rows' => $this->rows($jobs)]);
    }


    protected function rows($jobs)
    {
        $rows = [];

        foreach ($jobs as $job) {

            $rows[] = [
                'id' => $job->id,
                'created_at' => Carbon::createFromTimestamp($job->created_at)->format('Y-m-d H:i:s'),
                'available_at' => Carbon::createFromTimestamp($job->available_at)->format('Y-m-d H:i:s'),
                'queue' => $job->queue,
                'attempts' => $job->attempts,
                'object' => $this->display_manager->getOutstandingJobObject($job),
                'variables' => $this->display_manager->getOutstandingJobVariables($job),
                'options' => $this->optionButtons($job->id)
            ];
        }

        return $rows;
    }


    /**
     * Get the option buttons
     * @param int $job_id
     * @return string
     */
    protected function optionButtons(int $job_id): string
    {
        return '<div class="btn-toolbar">
                                     <a href="#" class="btn btn-xs btn-primary js-dispatch-now" data-job_id="' . $job_id . '">
        Set To Now
    </a>
                                     <a href="#" class="btn btn-xs btn-danger js-delete" data-job_id="' . $job_id . '">
        Delete
                                     </a>
                                 </div>';
    }
}
