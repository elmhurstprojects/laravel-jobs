<?php

namespace ElmhurstProjects\Jobs\Http\Controllers\Drilldowns;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use ElmhurstProjects\Jobs\Managers\DisplayManager;
use ElmhurstProjects\Jobs\Models\JobCompleted;
use Global4Communications\Payment\Models\PaymentTransaction;
use Illuminate\Http\Request;

class JobsCompletedController extends Controller
{
    protected $display_manager;

    public function __construct()
    {
        $this->display_manager = new DisplayManager();
    }

    public function json(Request $request)
    {
        $start_date = Carbon::createFromFormat('Y-m-d H:i', $request->get('start_date'))->startOfDay();

        $end_date = Carbon::createFromFormat('Y-m-d H:i', $request->get('end_date'))->endOfDay();

        $jobs = JobCompleted::whereBetween('created_at', [$start_date->format('Y-m-d H:i:s'), $end_date->format('Y-m-d H:i:s')])
            ->when($request->get('queue') != 'all', function ($query) use ($request) {
                return $query->where('queue', $request->get('queue'));
            })
            ->get();

        return response()->json(['request' => $request->all(), 'rows' => $this->rows($jobs)]);
    }


    protected function rows($jobs)
    {
        $rows = [];

        foreach ($jobs as $job) {
            $rows[] = [
                'id' => $job->id,
                'completed_at' => $job->created_at->toDateTimeString(),
                'queue' => $job->queue,
                'object' => $job->job,
                'variables' => $this->display_manager->getCompletedJobVariables($job),
            ];
        }

        return $rows;
    }
}
