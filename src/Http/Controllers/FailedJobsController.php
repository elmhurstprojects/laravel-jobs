<?php

namespace ElmhurstProjects\Jobs\Http\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use ElmhurstProjects\Jobs\Managers\FailedJobManager;
use ElmhurstProjects\Jobs\Managers\OutstandingJobManager;
use ElmhurstProjects\Jobs\Models\Job;
use ElmhurstProjects\Jobs\Models\JobCompleted;
use Illuminate\Http\Request;

class FailedJobsController extends Controller
{
    protected $failed_job_manager;

    public function __construct()
    {
        $this->failed_job_manager = new FailedJobManager();
    }

    public function failed()
    {
        $start_date = Carbon::now()->startOfDay()->format('Y-m-d H:i');

        $end_date = Carbon::now()->endOfDay()->format('Y-m-d H:i');

        return view('jobs::failed.index')
            ->with('request', (object)['start_date' => $start_date, 'end_date' => $end_date])
            ->with('queues', $this->failed_job_manager->getFailedQueues());
    }

    public function failedPost(Request $request)
    {
        return view('jobs::failed.index')
            ->with('request', (object)$request->all())
            ->with('queues', $this->failed_job_manager->getFailedQueues());
    }
}
