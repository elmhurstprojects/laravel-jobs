<?php

namespace ElmhurstProjects\Jobs\Http\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use ElmhurstProjects\Jobs\Managers\CompletedJobManager;
use ElmhurstProjects\Jobs\Managers\FailedJobManager;
use ElmhurstProjects\Jobs\Managers\OutstandingJobManager;
use ElmhurstProjects\Jobs\Models\Job;
use ElmhurstProjects\Jobs\Models\JobCompleted;
use Illuminate\Http\Request;

class CompletedJobsController extends Controller
{
    protected $completed_job_manager;

    public function __construct()
    {
        $this->completed_job_manager = new CompletedJobManager();
    }

    public function completed()
    {
        $jobs = JobCompleted::whereBetween('created_at', [Carbon::now()->startOfDay()->toDateTimeString(), Carbon::now()->endOfDay()->toDateTimeString()])->orderby('created_at', 'desc')->paginate(100);

        return view('jobs::completed.index')
            ->with('jobs', $jobs)
            ->with('queues', $this->completed_job_manager->getCompletedQueues());
    }

    public function completedPost(Request $request)
    {
        $start_date = ($request->get('start_date') != '') ? Carbon::createFromFormat('Y-m-d H:i', $request->get('start_date'))->startOfDay() : Carbon::now()->subCentury();

        $end_date = ($request->get('end_date') != '') ? Carbon::createFromFormat('Y-m-d H:i', $request->get('end_date'))->endOfDay() : Carbon::now()->endOfDay();

        $jobs = JobCompleted::whereBetween('created_at', [$start_date, $end_date])
            ->orderby('created_at', 'desc')
            ->paginate(100);

        return view('jobs::completed.index')
            ->with('jobs', $jobs)
            ->with('request', (object)$request->all())
            ->with('queues', $this->completed_job_manager->getCompletedQueues());;
    }
}
