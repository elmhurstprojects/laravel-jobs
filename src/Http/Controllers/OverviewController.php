<?php

namespace ElmhurstProjects\Jobs\Http\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use ElmhurstProjects\Jobs\Managers\OverviewManager;
use Illuminate\Http\Request;

class OverviewController extends Controller
{
    protected $overview_manager;

    public function __construct()
    {
        $this->overview_manager = new OverviewManager();
    }

    public function overview()
    {   $start_date = Carbon::now()->startOfDay();

        $end_date = Carbon::now()->addYear()->endOfDay();

        $overview = $this->overview_manager->getOverview($start_date, $end_date);

        return view('jobs::overview.index')
            ->with('request', (object)['start_date' => $start_date->format('Y-m-d H:i'), 'end_date' => $end_date->format('Y-m-d H:i')])
            ->with('pending_job_queue_breakdown', $overview->pending_job_queue_breakdown)
            ->with('pending_job_total', $overview->pending_job_total)
            ->with('stalled_job_queue_breakdown', $overview->stalled_job_queue_breakdown)
            ->with('stalled_job_total', $overview->stalled_job_total)
            ->with('failed_job_queue_breakdown', $overview->failed_job_queue_breakdown)
            ->with('failed_job_total', $overview->failed_job_total)
            ->with('completed_job_queue_breakdown', $overview->completed_job_queue_breakdown)
            ->with('completed_job_total', $overview->completed_job_total);
    }
}
