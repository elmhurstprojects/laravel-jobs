<?php

namespace ElmhurstProjects\Jobs\Http\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use ElmhurstProjects\Jobs\Managers\OutstandingJobManager;
use ElmhurstProjects\Jobs\Models\Job;
use Illuminate\Http\Request;

class OutstandingJobsController extends Controller
{
    protected $outstanding_job_manager;

    public function __construct()
    {
        $this->outstanding_job_manager = new OutstandingJobManager();
    }

    public function outstanding()
    {
        $start_date = Carbon::now()->subYears(10)->format('Y-m-d H:i');

        $end_date = Carbon::now()->addYears(10)->format('Y-m-d H:i');

        return view('jobs::outstanding.index')
            ->with('request', (object)['start_date' => $start_date, 'end_date' => $end_date])
            ->with('queues', $this->outstanding_job_manager->getQueues());
    }

    public function outstandingPost(Request $request)
    {
         return view('jobs::outstanding.index')
            ->with('request', (object)$request->all())
            ->with('queues', $this->outstanding_job_manager->getQueues());
    }

    public function dispatchOverride(Request $request)
    {
        $job = Job::find($request->get('job_id'));

        $job->save(['timestamps' => false]);

        $job->update(['available_at' => Carbon::now()->subMinute()->timestamp]);

        return response()->json([
            'request' => $request->all(),
            'job' => $job
        ]);
    }

    public function deleteJob(Request $request)
    {
        $job = Job::find($request->get('job_id'));

        $job->delete();

        return response()->json([
            'request' => $request->all(),
            'job' => $job
        ]);
    }
}
