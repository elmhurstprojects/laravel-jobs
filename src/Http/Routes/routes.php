<?php

Route::group(['middleware' => ['web', 'auth']], function () {
    Route::get('/jobs/overview', ['as' => 'jobs.overview', 'uses' => '\ElmhurstProjects\Jobs\Http\Controllers\OverviewController@overview']);
    Route::post('/jobs/overview', ['as' => 'jobs.overview.post', 'uses' => '\ElmhurstProjects\Jobs\Http\Controllers\OverviewController@overviewPost']);

    Route::get('/jobs/outstanding', ['as' => 'jobs.outstanding', 'uses' => '\ElmhurstProjects\Jobs\Http\Controllers\OutstandingJobsController@outstanding']);
    Route::post('/jobs/outstanding', ['as' => 'jobs.outstanding.post', 'uses' => '\ElmhurstProjects\Jobs\Http\Controllers\OutstandingJobsController@outstandingPost']);

    Route::post('/jobs/dispatch-now', ['as' => 'jobs.dispatch-now', 'uses' => '\ElmhurstProjects\Jobs\Http\Controllers\OutstandingJobsController@dispatchOverride']);

    Route::post('/jobs/delete', ['as' => 'jobs.delete', 'uses' => '\ElmhurstProjects\Jobs\Http\Controllers\OutstandingJobsController@deleteJob']);

    Route::get('/jobs/failed', ['as' => 'jobs.failed', 'uses' => '\ElmhurstProjects\Jobs\Http\Controllers\FailedJobsController@failed']);
    Route::post('/jobs/failed', ['as' => 'jobs.failed.post', 'uses' => '\ElmhurstProjects\Jobs\Http\Controllers\FailedJobsController@failedPost']);

    Route::get('/jobs/completed', ['as' => 'jobs.completed', 'uses' => '\ElmhurstProjects\Jobs\Http\Controllers\CompletedJobsController@completed']);
    Route::post('/jobs/completed', ['as' => 'jobs.completed.post', 'uses' => '\ElmhurstProjects\Jobs\Http\Controllers\CompletedJobsController@completedPost']);

    Route::get('/drilldowns/jobs-outstanding', ['as' => 'drilldowns.jobs-outstanding', 'uses' => '\ElmhurstProjects\Jobs\Http\Controllers\Drilldowns\JobsOutstandingController@json']);
    Route::get('/drilldowns/jobs-failed', ['as' => 'drilldowns.jobs-failed', 'uses' => '\ElmhurstProjects\Jobs\Http\Controllers\Drilldowns\JobsFailedController@json']);
    Route::get('/drilldowns/jobs-pending', ['as' => 'drilldowns.jobs-pending', 'uses' => '\ElmhurstProjects\Jobs\Http\Controllers\Drilldowns\JobsPendingController@json']);
    Route::get('/drilldowns/jobs-stalled', ['as' => 'drilldowns.jobs-stalled', 'uses' => '\ElmhurstProjects\Jobs\Http\Controllers\Drilldowns\JobsStalledController@json']);
    Route::get('/drilldowns/jobs-completed', ['as' => 'drilldowns.jobs-completed', 'uses' => '\ElmhurstProjects\Jobs\Http\Controllers\Drilldowns\JobsCompletedController@json']);

});

