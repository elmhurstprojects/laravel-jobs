<?php

namespace ElmhurstProjects\Jobs\Traits;

trait GetVariablesTrait
{
    /**
     * Extract the variables from the completed job
     * @param string $payload
     * @return array|null
     */
    protected function getVariablesFromCompletedJobPayload($payload): array
    {
        try {
            $payload = unserialize($payload['data']['command']);

            $variables = get_object_vars($payload);

            return $this->unsetGenericVariables($variables);
        } catch (\Exception $e) {
            return ['Variables Not Known' => $e->getMessage()];
        }
    }

    /**
     * Extract the variables from the outstanding job
     * @param string $payload
     * @return array|null
     */
    protected function getVariablesFromOutstandingJobPayload(string $payload): array
    {
        try {
            $payload = json_decode($payload);

            $payload = unserialize($payload->data->command);

            $variables = get_object_vars($payload);

            if (isset($variables['event'])) return $this->getEventVariables($variables['event']);

            return $this->unsetGenericVariables($variables);
        } catch (\Exception $e) {
            return ['Variables Not Known' => $e->getMessage()];
        }
    }

    /**
     * Returns the event vars as array
     * @param $event_variables
     * @return array
     */
    protected function getEventVariables($event_variables): array
    {
        $variables = (array)$event_variables;

        $variables['event'] = get_class($event_variables);

        return $variables;
    }

    /**
     * Remove useless variables
     * @param array $variables
     * @return array
     */
    protected function unsetGenericVariables(array $variables): array
    {
        unset($variables['tries']);
        unset($variables['timeout']);
        unset($variables['connection']);
        unset($variables['queue']);
        unset($variables['chainConnection']);
        unset($variables['chainQueue']);
        unset($variables['delay']);
        unset($variables['chained']);
        unset($variables['__PHP_Incomplete_Class_Name']);
        unset($variables['__PHP_Incomplete_Class']);
        unset($variables['job']);
        unset($variables['*job']);

        return $variables;
    }
}